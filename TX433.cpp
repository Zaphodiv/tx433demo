#include "TX433.h"

/**
 * @brief Bitbang data to 433MHz (or similar) transmitter.
 * This needs to be compatible with ATTiny85 and ATMega family microcontrollers and ESP8266/ESP32 in an ideal world.
 * 
 * It uses a timer ISR to generate reasonably accurate timing - there is a bit of slop in the protocol.
 * 
 * @Warning: IWFM/YMMV - it works for me, your mileage may vary. The decoded protocol sends a MARK (signal high) and SPACE (signal low) before sending 40 bits of data, followed by a brief MARK and long SPACE.
 * 
 * This works on an Arduino UNO and uses direct port access and timer manipulation. 
 *  */

#if defined(__AVR_ATtinyX5__)
ISR(TIMER1_OVF_vect);
#define USE_TIMER_0
#define TIMER_8_BIT
#else
#define USE_TIMER_2

#if defined(USE_TIMER_2)
ISR(TIMER1_OVF_vect);
#define TIMER_8_BIT

#else
ISR(TIMER1_OVF_vect);
#define TIMER_16_BIT

#endif // USER_TIMER_2

#endif

static void StartTx(uint8_t *bits);

// Static members - basically we are using a class for syntactic sugar
bool Tx433::_running = false;
TxEndFn Tx433::_handleTxStart = NULL;
TxEndFn Tx433::_handleTxEnd = NULL;
bool Tx433::_sentTxEnd = false;

/* Bit bang data to 433MHz transmitter using 16-bit timer - ATMEGA specific */
#define SET_TX_HI (PORTD |= _BV(PB5))
#define SET_TX_LO (PORTD &= ~_BV(PB5))

/**
 * @brief 433MHz protocol description
 * 
 * Data is 40 bit 
 * /code{.unparsed}
 *   ccccffff    00100000    00110000    0vvvvvvv    FFFFCCCC
 * /endcode
 * Where:
 * - `cccc` = channel number (`CCCC` is inverted and reversed)
 * - `ffff` = function (`FFFF` is inverted and reversed)
 *   - `0001` - Zap
 *   - `0010` - Buzz
 *   - `0100` - Beep
 *   - `1000` - LED
 * - `vvvvvvv` = value (0...100) Ignored for Beep/LED
 *
 * Timings are approximate but seem to work
 * 
 * ```
 *   ____________           ________     ....  ________      _
 * _/            \_________/____\\\\\___ .... /____\\\\\____/ \_______________________________
 * |                       |                                |                                |
 * | Preamble ~1550/800us  | Data (40 bits) 1: 750/200us  * |  Guard time ~13300us           | 
 * |                       |                0: 250/800us  * |                                |
 * ```
 * measured times, roughly. Seems to be some leeway in these figures
 * [modified timings https://github.com/CrashOverride85/collar ]
 */

/* For reference before using better macros (using 1/64 prescaler on 16-bit timer)
#define US_STARUP         (65536  -   1) // should be just one clock interval before we interrupt

#define US_PREAMBLE_MARK  (65536 -  389)  // 1550 (1200) [1550] !! 1556
#define US_PREAMBLE_SPACE (65536 -  197)  //  800  (800) [ 660] !! 788

#define US_1_MARK         (65536 -  195)  //  750  (600) [ 830] !! 772-784
#define US_1_SPACE        (65536 -   64)  //  200  (200) [ 200] !! 252-264

#define US_0_MARK         (65536 -   65)  //  250  (200) [ 360] !! 248-268
#define US_0_SPACE        (65536 -  191)  //  800  (600) [ 670] !! 756-780

#define US_END_HI         (65536 -   64)  // blip at the end    

#define US_GUARD          (65536 -  3233)  // 13300 (1200)
*/

#if defined(TIMER_16_BIT) // 16-bit timer
#define TIMER_COUNT TCNT1
#define TIMER_PRESCALER 64
#define TIMER_RESOLUTION 4
#define TIMER_MAXCOUNT 65536
#define TIMER_ISR_VECTOR TIMER1_OVF_vect
#else // 8 bit timer
#define TIMER_COUNT TCNT2
#define TIMER_PRESCALER 256
#define TIMER_RESOLUTION 16
#define TIMER_MAXCOUNT 256
#define TIMER_ISR_VECTOR TIMER2_OVF_vect
#endif

#define TIMER_US(x) (TIMER_MAXCOUNT - ((int)((x + TIMER_RESOLUTION) / TIMER_RESOLUTION) % TIMER_MAXCOUNT))
#define TIMER_RETRIES(x) (((int)((x + TIMER_RESOLUTION) / TIMER_RESOLUTION) / TIMER_MAXCOUNT))

#define US_STARUP TIMER_US(4)

#define US_PREAMBLE_MARK TIMER_US(1556)
#define US_PREAMBLE_SPACE TIMER_US(788)

#define US_1_MARK TIMER_US(780)
#define US_1_SPACE TIMER_US(260)

#define US_0_MARK TIMER_US(260)
#define US_0_SPACE TIMER_US(780)

#define US_END_HI TIMER_US(256)
#define US_GUARD TIMER_US(12932)
#define US_GUARD_RETRIES TIMER_RETRIES(12932)
#define US_ONE_REPEAT (57136) // how long does one message last?
// since US_GUARD will overflow 8-bit timer, add some retries

/**
 * @brief State model
 * 
 */
enum ETxState
{
  SS_IDLE,           ///< Idle state, not busy.
  SS_PREAMBLE_MARK,  ///< Sending preamble (MARK) - start of transmission, set busy flag.
  SS_PREAMBLE_SPACE, ///< Sending preamble (SPACE)
  SS_DATA_MARK,      ///< Sending data MARK.  Each data bit has a mark + space
  SS_DATA_SPACE,     ///< Sending data SPACE.
  SS_END_MSG,        ///< End of message bits. Send MARK for US_END_HI microseconds
  SS_GUARD_TIME,     ///< Guard timeout before we can send next message (extra state for counting down overflows)
  SS_END_TX          ///< Completed sending message, safe to repeat message if necessary. If there are no repeats, clear busy flag.
};

const uint8_t MESSAGE_BITS = 40; // 40 bits in total

volatile uint32_t usStart = 0L;
volatile uint32_t usDone = 0L;
volatile uint8_t timerPreGuardCount = 0;
static volatile uint8_t repeat = 0;
volatile ETxState txState = SS_IDLE;
volatile uint8_t currentBitPos = 0;

/**
 * @brief internal busy flag set when transmit starts, reset when transmit completed.
 * 
 * Must be volatile because it is reset by ISR
 */
volatile static bool bTransmitting = false;

/**
 * @brief data bits to send 
 */
uint8_t messageData[MESSAGE_BITS / 8];

#define MESSAGE_1 0x20 ///< Fixed value
#define MESSAGE_2 0x30 ///< Fixed value

Tx433::Tx433()
{

}

Tx433::Tx433(uint8_t pin, TxStartFn fpTxStart, TxEndFn fpTxEnd)
{
  _handleTxStart = fpTxStart;
  _handleTxEnd = fpTxEnd;
}

void Tx433::onTick(void)
{
  
  // are we waiting, transmitting, or is there noth

  if (!isBusy())
  {
    _running = false;
    /// @todo only call this once per message. At the moment it is called repeatedly.
    if (_handleTxEnd && !_sentTxEnd)
    {
      _sentTxEnd = true;
      _handleTxEnd();
    }
  }
}

/**
 * @brief Check if busy
 * 
 * @return true transimission in progress
 * @return false free
 */
bool Tx433::isBusy(void)
{
  return bTransmitting;
}

/**
 * @brief transmit a message.
 * 
 * Message duration is multiple of 250ms for convenience ~ 3 repetitions .
 * 
 * If there is no message currently being transmitted, this will call the TXStart handler.
 * @param channel             Channel identifier. For convenience, enum has actual bit pattern.
 * @param messageType         Type of message.
 * @param value               Message parameter (0..100) 
 * @param durationMs          Duration of send (ms). This is converted into a number of resends.
 * @return true               Message transmission started.
 * @return false              Already sending another message.
 */
bool Tx433::sendMessage(ETxChannel channel, ETxMessageType messageType, uint8_t value, long durationMs)
{
  // who should set the busy flag?
  if (bTransmitting)
  {
    return false; // busy
  }
  else
  {
    bTransmitting = true; // flag busy in case someone else tries to send
    _running = true;
  }

  long durationUs = durationMs * 1000;
  repeat = durationUs < (3 * US_ONE_REPEAT) ? 3 : (durationUs / US_ONE_REPEAT); // three resends minium

  // debug messages
  Serial.print((channel) == TXC_1 ? "1," : ",2,");
  Serial.print(messageType == TXM_ZAP ? "1," : "0,");
  Serial.print(value);
  Serial.print(",");
  Serial.println(durationMs);

  messageData[0] = channel | (messageType & 0x0F);
  messageData[1] = MESSAGE_1;
  messageData[2] = MESSAGE_2;
  messageData[3] = (value > 100) ? 100 : value; // only applies to TXM_BUZZ or TXM_ZAP but OK to send in all cases
  messageData[4] = 0x00;

  // this bit of nastiness takes messageData[0], reverses the bits, then inverts it to create msg[4]
  for (int b = 0; b < 8; b++)
  {
    messageData[4] |= !((messageData[0] & (0x80 >> b)) >> (7 - b)) << b;
  }

  StartTx(messageData);

  _sentTxEnd = false;
  if (_handleTxStart)
    _handleTxStart();

  return true;
}

static void StartTx(uint8_t *bits)
{

  for (int i = 0; i < MESSAGE_BITS / 8; i++)
    messageData[i] = bits[i];

  txState = SS_PREAMBLE_MARK;

#if defined(USE_TIMER_1)
  noInterrupts(); // critical section - disable all interrupts
  {
    timerPreGuardCount = US_GUARD_RETRIES; // Allow for overflow of 8-bit timer
    TIMER_COUNT = 0;                       // initialize timer1 registers
    TCCR1A = 0;                            // Normal operation - output ports disconnected
    TCCR1B = 0;                            // Normal mode = not PWM pr CTC
    TCCR1B |= (1 << CS11) | (1 << CS10);   // /64 prescaler .. 16MHz, each tick is 4uS
    TIMER_COUNT = US_STARUP;               // by having a short timeout, we will trigger the ISR
  }
  interrupts(); // enable all interrupts

  TIMSK1 |= (1 << TOIE1); // enable timer overflow interrupt
#else
  //TODO - make this use Timer2
  noInterrupts(); // critical section - disable all interrupts
  {
    timerPreGuardCount = US_GUARD_RETRIES; // Allow for overflow of 8-bit timer
    TCNT2 = 0;                             // initialize timer1 registers
    TCCR2A = 0;                            // Normal operation - output ports disconnected
    TCCR2B = 0;                            // Normal mode = not PWM pr CTC
    TCCR2B |= (1 << CS22) | (1 << CS21);   // /256 prescaler .. 16MHz, each tick is 16uS
    TCNT2 = US_STARUP;                     // by having a short timeout, we will trigger the ISR
  }
  interrupts(); // enable all interrupts

  TIMSK2 |= (1 << TOIE2); // enable timer overflow interrupt
#endif

}

/** @brief Interrupt service routine for the hardware timer.
 * 
 *  This does the nasty bit of sending bits out.
 *  
 * Although it seems to be quite a bit of code, in practice we are doing very little work each time:
 * 1. Setting/clearing the output bit high or low
 * 2. Setting the timer count (we get an interrupt when TIMER_COUNT reaches zero)
 * 3. Setting the next state.
 * 
 * - SS_DATA_MARK and SS_DATA_SPACE read the current bit (byte offset and shift operation).
 * - SS_DATA_SPACE also increments the bit count .
 * - SS_END_TX stops the timer and clears the busy flag.
 * 
 */
ISR(TIMER_ISR_VECTOR) // interrupt service routine
{
  static volatile bool bitValue = false;

  switch (txState)
  {
  case SS_PREAMBLE_MARK:
    usStart = micros();
    currentBitPos = 0;              // leftmost bit will be sent first
    SET_TX_HI;                      // set/clear output bit
    TIMER_COUNT = US_PREAMBLE_MARK; // stay in this state for a period
    txState = SS_PREAMBLE_SPACE;    // then go to the next state
    break;

  case SS_PREAMBLE_SPACE:
    SET_TX_LO;
    TIMER_COUNT = US_PREAMBLE_SPACE;
    txState = SS_DATA_MARK;
    break;

  case SS_DATA_MARK:
    // Since this is a data bit, need to know how long depending on whether it is a 0 or 1
    bitValue = (messageData[(currentBitPos >> 3) & 0x7] & (0x80 >> (currentBitPos & 0x07))) > 0;
    SET_TX_HI;
    TIMER_COUNT = (bitValue) ? US_1_MARK : US_0_MARK;
    txState = SS_DATA_SPACE;
    break;

  case SS_DATA_SPACE: // we were sending a data mark - now send its space
    SET_TX_LO;
    TIMER_COUNT = (bitValue) ? US_1_SPACE : US_0_SPACE;

    if (++currentBitPos < MESSAGE_BITS) // if we still have bits to send go back for next bit
      txState = SS_DATA_MARK;
    else
      txState = SS_END_MSG;
    break;

  case SS_END_MSG: // Sent all the data bits, now hold high for a while to mark the end
    SET_TX_HI;
    TIMER_COUNT = US_END_HI;
    txState = SS_GUARD_TIME;
    break;

  case SS_GUARD_TIME: // Send SPACE for a while - this is quite a long timer, so may overflow 8-bit timer
    SET_TX_LO;
    if (timerPreGuardCount > 0)
    {
      txState = SS_GUARD_TIME; // no state change
      TIMER_COUNT = 0;         // full period
      timerPreGuardCount--;
    }
    else
    {
      txState = SS_END_TX;
      TIMER_COUNT = US_GUARD; //start guard time
    }
    break;

  case SS_END_TX: // We have now finished guard time and are ready - stop the clock
    if (repeat > 1)
    {
      repeat--;
      txState = SS_PREAMBLE_MARK;
    }
    else
    {
#if defined(USE_TIMER_1)
      TIMSK1 &= ~(1 << TOIE1); // disable timer overflow interrupt
#else
      TIMSK2 &= ~(1 << TOIE2); // disable timer overflow interrupt
#endif
      txState = SS_IDLE;
      bTransmitting = false;
    }
    break;
  }
}

/* Timer notes:
 * Arduino Uno - 16MHz clock
 *               cannot use Timer 0 - used by millis() and delay() functions
 *               can use 16-bit Timer 1 if we don't use tone()
 *               can use  8-bit TImer 2 but some interaction with tone()
 * 
 * ATTiny86     
 * 
 * ESP32
 * 
 * ESP8266    - cannot use Timer 0 - used by wifi functions
 *              can use Timer 1 - but this may interfere with millis() and delay()
 * setup:
 * ```
 * timer1_attachInterrupt(onTimerISR);
 *  timer1_enable(TIM_DIV16, TIM_EDGE, TIM_SINGLE);
 *  timer1_write(600000); //120000 us
 * 
 * void ICACHE_RAM_ATTR onTimerISR(){
 *  digitalWrite(LED,!(digitalRead(LED)));  //Toggle LED Pin
 *  timer1_write(600000);//12us
 * }
 * ```
*/
