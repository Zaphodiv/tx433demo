#include "TX433.h"

static const char* PROGRAM_NAME = "tx433demo";
static const uint8_t TX_OUT = 5; // transmitter output pin

void onTxEnded(void);
void onTxStarted(void);
Tx433 TX(TX_OUT, onTxStarted, onTxEnded);

bool ready = false;
/** Visual feedback that transmission has started */
void onTxStarted(void)
{
  digitalWrite(LED_BUILTIN, HIGH);
  ready = false;
}

/** Visual feedback that transmission has ended */
void onTxEnded(void)
{
  digitalWrite(LED_BUILTIN, LOW);
  ready = true;
}

static uint32_t nextMs = millis() - 1;
void setup()
{
  Serial.begin(115200);
  Serial.print(PROGRAM_NAME);
  Serial.print(" ");
  Serial.print(__DATE__);
  Serial.print(" ");
  Serial.println(__TIME__);

  randomSeed(analogRead(A3));

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  ready = true;
  nextMs = millis();
  Serial.println("Ready!");
}

void loop()
{
  TX.onTick(); // run the transmitter state machine
    
  // send a random duration buzz periodically
  if (ready && (nextMs < millis()))
  {
    uint32_t lastMs = millis();
    int durationMs = 250 * (random(0,10) + 1);
    int delaybeforeNext = 500 * (random(0,10) + 1);
    nextMs = lastMs + durationMs + delaybeforeNext;
    
    TX.sendMessage(TXC_1, TXM_BUZZ, 5, durationMs );
  }
}
