#if !defined(_TX433_H_)
#define _TX433_H_

#include <Arduino.h>

// RF_TX is pin 5 but we use direct pin access

enum ETxMessageType
{
  TXM_LED = 0x08,
  TXM_BEEP = 0x04,
  TXM_BUZZ = 0x02,
  TXM_ZAP = 0x01
};

enum ETxChannel
{
  TXC_1 = 0xF0,
  TXC_2 = 0x80
};

typedef void (*TxEndFn)(void);
typedef void (*TxStartFn)(void);

class Tx433
{
  static TxStartFn _handleTxStart;
  static TxEndFn _handleTxEnd;
  static bool _running;
  static bool _sentTxEnd;
public:
  Tx433();
  Tx433(uint8_t pin, TxStartFn fpTxStart, TxEndFn fpTxEnd);
  static bool sendMessage(ETxChannel channel, ETxMessageType messageType, uint8_t value, long durationMs = 250);
  static void onTick(void);
  static bool isBusy(void);

  void attachTxStart(TxStartFn f) { _handleTxStart = f; }
  void attachTxEnd(TxEndFn f) { _handleTxEnd = f; }
};

#endif
