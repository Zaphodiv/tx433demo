# Tx433Demo

Demonstration of a state-machine driven 433MHz transmitter

This demo project creates a class (Tx433) that encapsulates the timing of a 433MHz protocol.

The details of the protocol are described in source code.

## Hardware

- Arduino UNO (doesn't work on a Mega because of port manipulation)
- 433MHx transmitter ([MX-FS-03](https://hobbycomponents.com/wired-wireless/168-433mhz-wireless-modules-mx-fs-03v-mx-05))


| Rx Connection | Arduino connection |
| ------------- | ------------------ |
| Vcc           | 5V                 |
| GND           | GND                |
| DATA          | Digital pin 5      |

## Software

See `Tx433Demo.ino`  for sketch. This uses `TX433.h` and `TX433.cpp`

Instructions:
1. Connect the transmitter to pin 5 on an Arduino Uno (or similar, maybe)

2. The built-in LED will flash whenever a message is being transmitted. 

3. Pair the receiver device - this should happen when the next message is sent.

4. The receiver should respond to further messages.

5. You can change the message type and value see

   ```C++
   TX.sendMessage(TXC_1, TXM_BUZZ, 5, durationMs );
   ```

## Operation

This was an experiment in decoding the protocol of a 433MHz-based transmitter/receiver and replaying messages, implementing a state machine that uses a low-level timer ISR (interrupt service routine), and implementing an asynchronous event-based system.

We use an ISR so that the Arduino can do other things while we are waiting to do a state change. Also, since the timings are in the order hundreds of microseconds (not milliseconds) we need a fine-grain timer.

### Protocol and state model

**Note**: This is one protocol that my particular transmitter/receiver used. You may need different timings and data encoding for other transmitter/receiver pairs, such as remote controlled power sockets.

The protocol has several elements:

1. A preamble that lets the receiver prepare for receiving data.
2. A series of 40 bit that encode the message (channel, function, value, checksum)
3. A guard time after the end of the message.

The real transmitter sends a burst of at least three repeats of the message. If the button is held, it will continue to send the message.

Transmission is a series of mark (transmitting high value) and space (transmitting low value) pairs. The timing of the mark and space determines the function.

For example on my transmitter we have the following mark/space times: 

| Function   | Mark time (microseconds) | Space time (microseconds) |
| ---------- | ------------------------ | ------------------------- |
| Preamble   | 1556 `SS_PREAMBLE_MARK`  | 788 `SS_PREAMBLE_SPACE`   |
| Data High  | 780 `SS_DATA_MARK`       | 260 `SS_DATA_SPACE`       |
| Data Low   | 260 `SS_DATA_MARK`       | 780 `SS_DATA_SPACE`       |
| Guard time | 256 `SS_END_MSG`         | 12932 `SS_GUARD_TIME`     |

The state model is quite simple. Most states bitbang (set or clear) the transmit pin then set the timer to wait for a period of time.

`SS_DATA_MARK` and `SS_DATA_SPACE` timing depends on whether we are sending a high or low bit.

`SS_GUARD_TIME` may be longer that the time can manage if we use an 8-bit timer, so we may have to reset the timer more than once before the whole period expires.

`SS_END_TX` marks the end of a single send. A single message takes around 57mS and we send each message at least three times. It is more convenient to define the intended duration of the message stream  in milliseconds (multiples of 250ms - value derived experimentally) and let the `TX433` class calculate how many messages need to be sent.

![state model](docs/Tx433_state_model.png)

### Callbacks and events

The class user can register callback functions that will be called when we start sending the first message in a set,and after the last message in the set has been sent. In the example sketch we register the events by passing the address of a function when we call the TX433 class constructor.

```
#include "TX433.h"

static const uint8_t TX_OUT = 5; // transmitter output pin

void onTxEnded(void);
void onTxStarted(void);

Tx433 TX(TX_OUT, onTxStarted, onTxEnded);
```

 The functions are very simple in the example - we turn an LED on or off, and reset or set a `ready` flag.

```
/** Visual feedback that transmission has started */
void onTxStarted(void)
{
  digitalWrite(LED_BUILTIN, HIGH);
  ready = false;
}

/** Visual feedback that transmission has ended */
void onTxEnded(void)
{
  digitalWrite(LED_BUILTIN, LOW);
  ready = true;
}
```

